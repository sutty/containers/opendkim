FROM sutty/monit:latest
MAINTAINER "f <f@sutty.nl>"

RUN apk add --no-cache opendkim mini-sendmail
COPY ./monit.conf /etc/monit.d/opendkim.conf
COPY ./opendkimd.sh /usr/local/bin/opendkimd
COPY ./sendmail.sh /usr/bin/sendmail
RUN install -dm 2750 -o opendkim -g opendkim /run/opendkim

EXPOSE 8891
VOLUME /etc/opendkim
