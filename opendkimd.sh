#!/bin/sh

pid=/tmp/opendkim.pid

case $1 in
  stop) cat ${pid} | xargs kill ;;
  *)
    chown -R opendkim:opendkim /etc/opendkim
    /usr/sbin/opendkim ;;
esac
